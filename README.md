# UOSSH Readme #

Oct 2016 Ryan Leonard

This is a Bro script that uses the Bro SQLite logging framework to monitor and log for inbound ssh connections that have suspect geoIP mappings

See the summary at the top of main.bro for more information.

### How do I get set up? ###

Download this repository into <prefix>/share/bro/site. Then add the appropriate load directive to the local.bro file.


```
#!bash

cd /bro/share/bro/site
git clone https://terminalstderr@bitbucket.org/terminalstderr/uossh.git
echo "# monitor and raise notifications for inbound ssh connections that have suspect geoIP mappings" >> local.bro
echo "@load uossh" >> local.bro

```


### Script Parameters: Redefs ###

Can redefine the **dp_path**

EXAMPLE

redef UOSSH::db_path = /home/bro/logs/uossh;


### Who do I talk to? ###

Contact Ryan Leonard with any questions or concerns!

Ryan.Leonard71@gmail.com