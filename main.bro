##! Oct 2016 Ryan Leonard -- rleonar7@uoregon.edu
##! Logs inbound ssh connections to sqlite DB that have suspect geoIP mappings

# Goal1: Detect when a host inside our network is being logged into by suspect
#				ip address, i.e. outside of the Oregon region.
# Goal2: Detect when a malicious actor outside of the Oregon region is 
#				attempting to login to hosts inside the UO network
#
# This script simply logs all hosts with suspect ip addresses connecting to 
# hosts inside the UO network
# 
# This bro script will look at all successful ssh authentications. If the 
# following criteria are met, we add a log entry:
#   (0) authentication is successful
#		(1) originating host is not in the Oregon region 
#		(2) responding host is in our local network (e.g. UO network)

module UOSSH;

export 
	{
	##! A whitelist of country codes that we consider 'safe'. Empty implies that all country codes are safe
	const safe_country_codes: set[string] = {"US"} &redef;
	##! A whitelist of region codes that we consider 'safe'. Empty implies that all regions are safe
	const safe_regions: set[string] = {"OR"} &redef;
	##! A whitelist of city codes that we consider 'safe'. Empty implies that all cities are safe
	const safe_cities: set[string] = {} &redef;
	const db_path = "/tmp/uossh" &redef;
	}

function is_location_unsafe(loc: geo_location): bool
	{
	# Check if loc is in an unsafe country
	local loc_country_unsafe = (|safe_country_codes| > 0 &&
			(loc?$country_code && loc$country_code !in safe_country_codes));
	# Check if loc is in an unsafe region
	local loc_region_unsafe = (|safe_regions| > 0 &&
			(loc?$region && loc$region !in safe_regions));
	# Check if loc is in an unsafe city
	local loc_city_unsafe = (|safe_cities| > 0 &&
			(loc?$city && loc$city !in safe_cities));
	# Return True iff the location was either in an unsafe country, region, or city
	return loc_country_unsafe || loc_region_unsafe || loc_city_unsafe;
	}

# This function returns true iff 
#		1. the origin host geoip information is from an 'unsafe' location
#		2. the responding host has an internal (UO) ip address
function is_unsafe_to_internal(info: SSH::Info): bool
	{	
	local orig_location = lookup_location(info$id$orig_h);
	local is_origin_unsafe = is_location_unsafe(orig_location);
	local is_responder_internal = Site::is_local_addr(info$id$resp_h);
	return is_responder_internal && is_origin_unsafe;
	}

function is_successful_ssh_connection(info: SSH::Info): bool
	{
		return info$auth_success;
	}

# Currently the only criteria is that the connection is coming from unsafe area and aimed at an internal host
function uossh_criteria(info: SSH::Info): bool
	{
	return is_unsafe_to_internal(info);
	}

# has negaitve priority so that it is done after and default priority actions.
# this makes it so that redef of safe_* will take effect correctly
event bro_init() &priority=-10
  {
	local is_safezone_defined = (|safe_country_codes| > 0 || |safe_regions| > 0 || |safe_cities| > 0);

	if (is_safezone_defined)
		{
			local f = Log::get_filter(SSH::LOG, "default");
			f$name="uossh";
			f$path=db_path;
			f$config=table(["tablename"] = "uossh");
			f$writer=Log::WRITER_SQLITE;
			f$pred=uossh_criteria;
			f$exclude=set("direction");
			Log::add_filter(SSH::LOG, f);
		}
  }
